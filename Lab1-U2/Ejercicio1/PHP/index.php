<!DOCTYPE html>
<html class="no-js" lang="es">
<!-- encabezado -->
<head>
  <meta charset="UTF-8">
  <title>Ejercicio 1</title>
  <link rel="stylesheet" type="text/css" href="../CSS/estilo.css">
</head>
<!--cuerpo  de la página -->
<body>
    <h1>Tabla 10 x 10</h1>
    <table>
  <!-- se realiza la table con ayuda de php -->
  <?php
  echo ("</tr>\n");
  /* contador para realizar tabla*/
  $contador = 1;
  /*ciclos para hacer las filas y columanas 10x10*/
  for ($fila = 1;$fila <= 10; $fila++ ) {
    for ($columna = 1;$columna <= 10; $columna++ ) {
      echo ("<td>");
      echo $contador;
      echo ("</td>");
      $contador++;
    }
    echo ("</tr>\n");
  }
  ?>
  </table>
</body>
</html>
