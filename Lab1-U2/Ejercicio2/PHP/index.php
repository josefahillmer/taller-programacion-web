<!DOCTYPE html>
<html class="no-js" lang="es">
<!-- encabezado -->
<head>
  <meta charset="UTF-8">
  <title>Ejercicio 2</title>
  <link rel="stylesheet" type="text/css" href="../CSS/estilo.css">
</head>
<!--cuerpo  de la página -->
<body>
    <h1>Tabla N X M (30 x 30)</h1>
    <table>
      <!-- se realiza la table con ayuda de php -->
      <?php
      /* Se define el tamaño de la tabla y contador*/
      define('TAM', 30);
        $contador = 1;
        /*ciclos para hacer las filas y columanas NxM*/
        for ($fila = 1;$fila <= TAM; $fila++ ) {
          /*Si la fila es par se cambia de color a gris*/
          if ($fila % 2 == 0)
          echo "<tr bgcolor=grey>";
          else
          echo "<tr bgcolor=white>";
          for ($columna = 1;$columna <= TAM; $columna++ ) {
            echo ("<td>");
            echo $contador;
            echo ("</td>");
            $contador++;
          }
          echo ("</tr>\n");
        }
        ?>
    </table>
</body>
</html>
