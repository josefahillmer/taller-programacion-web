<!DOCTYPE html>
<html class="no-js" lang="es">
<!-- encabezado -->
<head>
  <!-- Para uso de caracteres -->
  <meta charset="UTF-8">
  <title>Ejercicio 3</title>
  <link rel="stylesheet" type="text/css" href="../CSS/estilo.css">
</head>
<!--cuerpo  de la página -->
<body>
    <div align="center">
    <h1>Creación de tabla</h1>
    <!-- Se llama el index para insertar el tamaño de la tabla y el color -->
    <form action="../PHP/index.php" method="GET">
        <a>Tamaño: </a>
        <input type="number" min = 1 placeholder="Inserta tamaño de la tabla" name="TAM"><br><br>
        <a>Color: </a>
        <input type="color" name ="color"><br><br>
        <input type="submit" value="Crear tabla">
    </form>
    <br>
    <table>
      <!-- se realiza la table con ayuda de php -->
      <?php
      /* Se define el tamaño de la tabla y color*/
      $TAM = $_GET['TAM'];
      $color = $_GET['color'];
      /* Se define el contador para la tabla*/
      $contador = 1;
        /*ciclos para hacer las filas y columanas NxM*/
        for ($fila = 1;$fila <=$TAM; $fila++ ) {
          /*Si la fila es par se cambia de color*/
          if ($fila % 2 == 0)
          echo "<tr bgcolor=$color>";
          else
          echo "<tr bgcolor=white>";
          for ($columna = 1;$columna <=$TAM; $columna++ ) {
            echo ("<td>");
            echo $contador;
            echo ("</td>");
            $contador++;
          }
          echo ("</tr>\n");
        }
        ?>
    </table>
</body>
</html>
