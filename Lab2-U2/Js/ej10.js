/* Escribe un programa que simule el lanzamiento de un dado. Utiliza imagenes para cada valor del dado
y alguna funcion random para simular el lanzamiento */

// Se crea la función para ir cambiando el dado
function dado() {
    // Se utiliza Math.random para generar un numero aleatorio del 1 al 6
    let azar = Math.floor((Math.random() * 6)+1);
    console.log(azar);
    //Se utiliza el azar guardado con anterioridad y se ven todos los posibles casos
    switch (azar) {
        case 1:
            // Cuando toca 1, se coloca su dado correspondiente
            document.getElementById('dados').src = "fotos/1.png";
            break;
        case 2:
            // Cuando toca 2, se coloca su dado correspondiente
            document.getElementById('dados').src = "fotos/2.png";
            break;
        case 3:
            // Cuando toca 3, se coloca su dado correspondiente
            document.getElementById('dados').src = "fotos/3.png";
            break;
        case 4:
            // Cuando toca 4, se coloca su dado correspondiente
            document.getElementById('dados').src = "fotos/4.png";
            break;
        case 5:
            // Cuando toca 5, se coloca su dado correspondiente
            document.getElementById('dados').src = "fotos/5.png";
            break;
        default:
            // Cuando toca 6, se coloca su dado correspondiente
            document.getElementById('dados').src = "fotos/6.png";
            break;
    }
}
