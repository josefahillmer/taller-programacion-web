/* Escribe un programa que muestre un proyector de fotografıas (5 fotos). La idea es tener dos botones
(anterior y siguiente) para ir mostrando las imagenes.*/

// Contador
var contador = 0;
// Se crea un arreglo con las imagenes
var imagenes = new Array("fotos/img1.jpg", "fotos/img2.jpg", "fotos/img3.jpg",
    "fotos/img4.jpg", "fotos/img5.jpg");

//Fucnion que ira a la siguiente imagen
function siguiente() {
    //Condicion de contador menor a 4 y si lo es se cuenta
    if (contador < 4) {
        contador ++;
        // Se cambia la imagen
        document.getElementById("cambio").src = imagenes[contador];
    }
}

//Funcion para retrodecer imagenes
function anterior() {
    // Como es la primera posicion es 0
    if (contador > 0) {
        // Se resta al contador ya que se retrocede
        contador --;
        // Se cambia la imagen
        document.getElementById("cambio").src = imagenes[contador];
    }
}
