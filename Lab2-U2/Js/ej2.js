/* Escribe un programa de tres líneas que pida un número, pida otro número y
escriba el resultado de sumar estos dos números*/

// Se crea la función que pedira los numeros y los sumara
function suma() {
    // Primer número
    let num1 = prompt("Suma de números\nIngrese un número: ");
    // Segundo número
    let num2 = prompt("Ingrese un número:");
    // Se suman los números y con parseInt se obtiene el valor
    alert("La suma es: " + (parseInt(num1) + parseInt(num2)));
}
// Se llama la función
suma();
