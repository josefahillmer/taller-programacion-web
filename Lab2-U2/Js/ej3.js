/* Escribe un programa que pida dos núumeros y escriba en la
pantalla cual es el mayor */

// Función que determina el número mayor
function mayor() {
    // Se pide al usuario ingresar los números
    let num1 = parseInt(prompt("Se buscará el número mayor\nIngrese el primer número: "));
    let num2 = parseInt(prompt("Ingrese el segundo número:"));

    // Si el numero 1 es mayor al 2, entoces va a hacer mayor
    if(num1 > num2){
      window.alert("El número mayor es: " + num1);
    }
    // Si el segundo número es mayor que el otro entonces el segundo es el mayor
    else if (num2 > num1) {
      window.alert("El número mayor es: " + num2)
    }
    // Si no es ninguno de los anterior es porque es el mismo número
    else {
      window.alert("Se ingreso el mismo número: " + num1)
    }
}
// Se llama la función
mayor();
