// Escribe un programa que pida una frase y escriba cuantas veces aparece la letra a.

// Se crea la función que contara las letra A
function letrasA() {
    // Se le pide al usuario ingresar una frase
    let frase = prompt("Se contaran las letras A\nIngrese una frase:");
    // Se crea un contador que almacenara la cantidad de letras A de la frase
    let contador = 0;
    // Se va a recorrer la frase
    for (let i = 0; i < frase.length; i++) {
        // Se crea una lista con la frase y ademas se deja toda la frase en mínuscula
        if (frase[i].toLowerCase() == "a") {
            // Se encuentra una a entonces se suma
            contador ++;
        }
    }
    alert("En la frase: " + frase + "\nSe encontraron: " + contador + " A");
}
// Se llama la función
letrasA();
