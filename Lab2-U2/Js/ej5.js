/* Escribe un programa que pida un núumero y diga si es divisible por 2 */

// Se crea la función para ver si el número es divisible
function divisible() {
    // Se le pide al usuario un número
    let num = prompt("Se verá si el número es divisible por 2\nIngrese un número: ");
    // Si el número es divido por 2 y es 0 entonces es divisible
    if (num % 2 == 0) {
        //Si el modulo de 2 es igual a cero
        alert("El número "  + num + " es divisible por dos");
        // Si no, no es divisible
    } else {
        alert("El número "  + num + " NO es divisile");
    }
}
// Se llama la función
divisible();
