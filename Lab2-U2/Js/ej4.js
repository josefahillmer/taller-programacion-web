/* Escribe un programa que pida 3 números y escriba en la pantalla el mayor de los tres*/

// Función que busca el mayor de los tres números
function mayor() {
    // Se le pide al usuario ingresar 3 números
    let num1 = prompt("Se buscará el número mayor\nIngrese el primer número: ");
    let num2 = prompt("Ingrese el segundo número: ");
    let num3 = prompt("Ingrese el tercer número: ");

    // Si el primer número es mayor que todos
    if (num1 > num2 && num1 > num3) {
        alert("El numero mayor es: " + num1);
      // Si el segundo número es mayor que todos
    } else if (num2 > num3) {
        alert("El numero mayor es: " + num2);
      // Si el tercer número es mayor que todos
    } else {
        alert("El numero mayor es: " + num3);
    }
}
// Se llama la función
mayor();
