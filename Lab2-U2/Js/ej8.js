// Escribe un programa que pida una frase y escriba cuantas veces aparecen cada una de las vocales.
// Se crea función que contara las vocales
function vocales() {
    // Contadores para cada vocal
    let contadorA = 0;
    let contadorE = 0;
    let contadorI = 0;
    let contadorO = 0;
    let contadorU = 0;
    // Se le pide al usuario ingresar una frase
    let frase = prompt("Se contaran las vocales\nIngrese una Frase:");
    // Se recorre el largo de la frase
    for (let i = 0; i < frase.length; i++) {
        // Se obtienen los distintos estados de la frase y se convierten las letras en mínusculas
        switch (frase[i].toLowerCase()) {
            // Se cuentan las letras A
            case "a":
                contadorA ++;
                //Se detiene el proceso
                break;
              // Se cuentan las letras E
            case "e":
                contadorE ++;
                //Se detiene el proceso
                break;
              // Se cuentan las letras I
            case "i":
                contadorI ++;
                //Se detiene el proceso
                break;
                // Se cuentan las letras O
            case "o":
                contadorO ++;
                //Se detiene el proceso
                break;
              // Se cuentan las letras U
            case "u":
                contadorU ++;
                //Se detiene el proceso
                break;
        }
    }
    /*Se imprime la cantidad de cada letra*/
    alert("En la frase: " + frase + "\nSe encontraron:\nA: " + contadorA +
          "\nE: " + contadorE +
          "\nI: " + contadorI +
          "\nO: " + contadorO +
          "\nU: " + contadorU);
}
// Se llama la función
vocales()
